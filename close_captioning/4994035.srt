1
00:00:01,009 --> 00:00:03,021
Pleases the Commission.

2
00:00:05,018 --> 00:00:07,027
This is very serious
and decisive moment in

3
00:00:07,027 --> 00:00:11,014
the history of the Catholic
Church in Australia.

4
00:00:14,005 --> 00:00:17,010
The sacred place of children,
their innocence and their

5
00:00:17,010 --> 00:00:20,019
trustfulness is central to
the Christian tradition and to

6
00:00:20,019 --> 00:00:22,022
the Catholic faith.

7
00:00:26,011 --> 00:00:30,024
Many will remember from their
own childhoods the ageless words

8
00:00:30,024 --> 00:00:33,015
from the Gospel of Mark.

9
00:00:34,026 --> 00:00:37,024
Let the little children come
to me.

10
00:00:45,012 --> 00:00:48,006
Do not stop them for it is
to such of these that the

11
00:00:48,006 --> 00:00:50,019
the kingdom of God belongs.

12
00:00:51,001 --> 00:00:53,009
And again from Mark,
driving home the point.

13
00:00:53,009 --> 00:00:54,020
Whoever causes one of these

14
00:00:54,020 --> 00:00:57,023
little ones who believe in me
to stumble, it would be better

15
00:00:57,023 --> 00:01:00,022
for him if a great millstone
were hung around his neck and

16
00:01:00,022 --> 00:01:03,003
he were cast into the sea.

17
00:01:08,025 --> 00:01:11,027
The Catholic Church comes
before this Royal Commission

18
00:01:11,027 --> 00:01:13,016
acutely aware of its failures

19
00:01:13,016 --> 00:01:16,023
in this fundamental part
of its mission.

20
00:01:21,010 --> 00:01:24,024
For many Catholics
the realisation that some

21
00:01:24,024 --> 00:01:29,009
Catholic priests and religious
of all people had betrayed

22
00:01:29,009 --> 00:01:33,022
the trust of children and their
parents, by abusing them

23
00:01:33,022 --> 00:01:37,029
in sexual way has been almost
unbearable.

24
00:01:43,005 --> 00:01:46,024
The further bleak realisation
that such behaviour was

25
00:01:46,024 --> 00:01:50,012
sometimes covered up,
with wrongdoers protected while

26
00:01:50,012 --> 00:01:53,025
victims we disbelieved or
treated coldly, has made

27
00:01:53,025 --> 00:01:57,025
an already disgraceful situation
even worse.

28
00:02:04,010 --> 00:02:07,024
For the vast majority of priests
and religious.

