1
00:00:01,013 --> 00:00:03,006
Chaos on the streets of Beirut

2
00:00:03,006 --> 00:00:06,028
as the city responds to
with deadly explosion.

3
00:00:07,016 --> 00:00:09,023
Witnesses say the blast killed

4
00:00:09,023 --> 00:00:13,020
at least 3 people and wounded
about 20.

5
00:00:13,020 --> 00:00:16,022
The blast happened in the Shiite
group has was southern Beirut

6
00:00:16,022 --> 00:00:20,006
stronghold charred cars
like twisted and blackened in

7
00:00:20,006 --> 00:00:23,020
the balconies and facades
of several buildings appear

8
00:00:23,020 --> 00:00:26,005
crumbled to the ground.

9
00:00:26,005 --> 00:00:29,002
Emergency workers rushed victims
to a local hospital where more

10
00:00:29,002 --> 00:00:32,026
crowds gathered the Lebanese
capital has been hit by a series

11
00:00:32,026 --> 00:00:36,022
of bombs of recent months one
blast last week.

12
00:00:36,022 --> 00:00:37,000
Kill the former minister

13
00:00:37,000 --> 00:00:39,028
and political adversary
of Hezbollah.

