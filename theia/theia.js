(function(window) {

		var digiData = window.digitalData || {},
        titan = digiData.titan || {},
        custParams = titan.adKeyValues || {},
		videoTitan = titan.video || {},
        page = digiData.page || {},
        pageInfo = page.pageInfo || {},
        videoSize = pageInfo.videoSize, //TBC since we can multi state sz value for video adcall
        adTester = titan.adTester || {},
        useAdTester,
        preSite = titan.adSite,
        mobileSite,
        adSite,
        adZone,
        videoSize = "640x360|4x1",
        logHolder = [];

    setup();

    // Functions to handle logs and errors in Titan

    // Log to be used for information
    function log(message) {
        // Check for localStorage to avoid crashes in non dom storage enviroments
        // localStorage value of titanLog = "true" required to show log
        if (localStorage && localStorage.theiaLog == "true") console.log("THEIA: " + message);
        logHolder.push(message);
    }

    // Error to be used to alert critical issues
    function error(message) {
        // Same as Log
        if (localStorage && localStorage.theiaLog == "true") console.error("THEIA: " + message);
        logHolder.push("e:" + message);
    };

    function configurePlayer() {
        //dynamicall pulls playerID in from video tag element. this is used within the adcall for metadata purposes
        var player = this,
            playerId = player.id(),
            videoId = player.el().getAttribute("data-video-id"),
            videoAccountId = player.el().getAttribute("data-account"),
            dfpCmsId = getCmsId(); // placeholder for final mechanism mapped above via videoAccountId
        
        // Do we need this logging?
        log("player is " + playerId);
        log("videoId is " + videoId);
        log("videoAccountId is " + videoAccountId);

        // checks if important values are present. if they are not it does not run and will not make an ad call. instead prints a console error
        if (videoSize && adSite) {


            // Do we need this logging?
            log("adSite pre adcall " + adSite);
            log("Setting server URL for: " + playerId);
			
			//---- OLI note
			//at this point of time, this is ENOUGH, we dont need to configure ALL the player as the javascript
			//is loaded for each player.... The only thing we need to do here is to grab the target and categories for each video
			var catArray = getVideoCategories(playerId);
			//---- End OLI note


            // Calls doubleclick video IMA (video SDK) and sets the adcall string to pass through
            player.ima3.settings.serverUrl = "http://pubads.g.doubleclick.net/gampad/ads?iu=/6411/" + adSite + adZone + "&sz=" + videoSize + "&cust_params=" + getCustParams(catArray,playerId) +"&impl=s&gdfp_req=1&env=vp&output=xml_vmap1&unviewed_position_start=1&url=[referrer_url]&correlator=[timestamp]&ad_rule=1&cmsid=" + dfpCmsId + "&vid=" + videoId + "&playerid=" + playerId;
		    //console.log(encodeURIComponent(player.ima3.settings.serverUrl));
            // Do we need this logging
            //log("Ad Call String " + player.ima3.settings.serverUrl);

            player.ads.debug = true;

        } else {
            error("Essential ad call params, either videoSize or adSite, are missing");

            // TBC - the following adcall only kicks in if key datalayer values or datalayer in general is not present. This covers shared / embedded players. note ad call below is set to a 'Not Brand safe' adSite
            // player.ima3.settings.serverUrl = "http://pubads.g.doubleclick.net/gampad/ads?iu=/6411/vid.drxvidembed&sz=" + videoSize + "&cust_params=embeddedplayer%3Dtrue&impl=s&gdfp_req=1&env=vp&output=xml_vmap1&unviewed_position_start=1&url=[referrer_url]&correlator=[timestamp]&playerid=" + playerId;
        }

        // Fix mobile devices
        if (videojs.TOUCH_ENABLED) player.el().className += ' vjs-mouse';
        // Sets timeout of player control bar to never. this should stop the default behaviour of minimising out of view after a short period of time
         // player.options().inactivityTimeout = 0;
         // player.on('userinactive', function(ev) {
        //      this.userActive(true);
        // });

    }

    //function to pull together all applicable key values for ad targeting purposes. sets up for cust_params in adcall, player.ima3.settings.serverUrl.
    function getCustParams(catArray,playerId) {
        var targeting = {},
            key,
            targetingOutput = "";

        for (key in custParams) {
            //ensures object prototype is not looked at
            if (custParams.hasOwnProperty(key)) {
                targeting[key] = custParams[key];
            }
        }

        // checks to ensure variable is present and sets value to be picked up by adcall, player.ima3.settings.serverUrl
        if (pageInfo.sysEnv) targeting.sysEnv = pageInfo.sysEnv;
        if (pageInfo.layout) targeting.layout = pageInfo.layout;
        if (pageInfo.pageID) targeting.pageID = pageInfo.pageID;
        for (var x = 0; x < catArray.length; x++){
            targeting['cat'+(x||"")] = catArray[x];
        }
        if (pageInfo.generator) targeting.generator = pageInfo.generator;
        if (adTester.adcallkw) targeting.adcallkw = adTester.adcallkw;
        if (titan.adExclusions) targeting.excl_cat = titan.adExclusions;
        if (titan.cmsRemove) targeting.cmsRemove = titan.cmsRemove;

        // Anthony - if cmsRemove is set in the DL also set dcds adsuppres which will suppress ad's pre roll
        if (titan.cmsRemove) targeting.blank = "dcdsadsuppress";

        for (key in targeting) {
            if (targeting.hasOwnProperty(key)) {
                // for each key and value added to string adds an '&' on the end
                if (targetingOutput) targetingOutput += "&";
                // inserts "=" between each key and Value
                targetingOutput += key + "=" + targeting[key];
            }

        }
		targetingOutput += '&playerpos=' + getPlayerPosition(playerId);
        // encodes string ready for adcall, player.ima3.settings.serverUrl

        return encodeURIComponent(targetingOutput);
    }

    function getCmsId() {
        /* ========== CODE NEEDED ========== */
        /*js to grab videoAccountId above and map to respective DFP ID associated via the API key*/
        /* ========== CODE NEEDED ========== */
        return '580';
    }; 
	
	// Will return true if if adtester should run on the page based of the query string and sessionStorage fallback
    function checkAdTester() {
        var adcall = /adcall[^=]{0,3}=[^&]*/ig,
            sessionStorage = window.sessionStorage,
            domQuery = sessionStorage ? sessionStorage.adtester : null,
            query = location.search.match(adcall) || (domQuery ? domQuery.match(adcall) : null),
            queryPeices,
            queryCounter;

        // Populate dataLayer if a query values found
        if (query) {

            // Loops through the matched query values and adds them to the dataLayer
            for (queryCounter = query.length - 1; queryCounter >= 0; queryCounter--) {
                queryPeices = query[queryCounter].split('=');
                adTester[queryPeices[0]] = queryPeices[1].split('_'); // split into array if an underscore exists
            }

            // Adds adTester values back the the DataLayer on the page
            titan.adTester = adTester;

        }

        // Sets flag to true if the minimum requirements are met, adcall and adcallkw are present 
        useAdTester = (isArray(adTester.adcall) && isArray(adTester.adcallkw));
    }

    // Test if an object is an array
    function isArray(arr) {
        return Object.prototype.toString.call(arr) === "[object Array]";
    }; // Sets up external links to functions so that they can be initated by the page
    function api() {
        // Theia version number
        theia.version = "0.0.2.3";
        // Adds setupAds function to video js
        videojs.plugin('setupAds', configurePlayer);
    };

    function setup() {
	    // Theia API Object
        window.theia = window.theia || {};

        // Temp version reporting
        log("0.0.2.3");

        // Check that adtester is in use
        checkAdTester();

        // Checks to define adSite and adZone
        mobileSite = preSite && preSite.match(/(mobile-|tablet-|vid\.|video-)(.*)/i);
        adSite = useAdTester ? "vid.adtester" : preSite && "vid." + (mobileSite ? mobileSite[2] : preSite);
        adZone = useAdTester ? "" : "/" + titan.adZone;

        // Set up the api methods
        api();
    };
	
	//return array of categories
	function getVideoCategories(playerId) {
       var catArray = [];
       for(var x = 0; x < videoTitan.length; x++) {
            if(videoTitan[x].videoId == playerId){				
				for( var name in videoTitan[x]){
					if( name.indexOf('cat') == 0){
						catArray.push(videoTitan[x][name]);
					}
				 }
				break;
			}            
        }	       
		return catArray;		
    };
	
	//return the playerPosition
	function getPlayerPosition(playerId) {
       var position = "";
       for(var x = 0; x < videoTitan.length; x++) {
            if(videoTitan[x].videoId == playerId){				
				for( var name in videoTitan[x]){
					if( name.indexOf('position') == 0){
						position = (videoTitan[x][name]);
						break;
					}
				 }
				break;
			}            
        }	       
		return position;		
    };
	
    

})(window);